using System.Net;
using Microsoft.AspNetCore.Mvc;
using IMS.API.DTOs;
using FalconFlexCommon.Extensions;

// using IMS.API.Extensions;
using IMS.API.RequestModels;
using IMS.Domain.Entities.Orders;
using IMS.API.Services.Interfaces;
using Serilog;
using SnoonuCommons.Helpers;

namespace IMS.API.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class OrderController : Controller
{
    private readonly IOrderService _orderService;

    public OrderController(IOrderService orderService)
    {
        _orderService = orderService;
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(OrderDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<OrderDto>> CreateAsync([FromBody] OrderDto orderDto)
    {
        try
        {
            var order = await _orderService.CreateAsync(orderDto);
            Log.Information($"Successfully created dataSource with id: {order.Id}");
            var newOrderDto = new OrderDto(order.OrderTime, order.OrderTotal, order.CustomerId, order.Status, order.Products);
            return Ok(newOrderDto);
        }
        catch (Exception e)
        {
            Log.Error($"Failed to create order. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(e.Message);
        }
    }
    
    
    [HttpPut]
    [ProducesResponseType(typeof(OrderDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<OrderDto>> UpdateAsync([FromBody] UpdateOrderRequest updateOrderRequest)
    {
        try
        {
            await _orderService.UpdateOrderDetails(updateOrderRequest);
            Log.Information($"Successfully updated order with id: {updateOrderRequest.Id}");
            var order = await _orderService.GetByIdAsync(updateOrderRequest.Id);
            var newOrderDto = new OrderDto(order.OrderTime, order.OrderTotal, order.CustomerId, order.Status, order.Products);
            return Ok(newOrderDto);
        }
        catch (Exception e)
        {
            Log.Error($"Failed to update order with id {updateOrderRequest.Id}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(e.Message);
        }
    }

    [HttpGet]
    [ProducesResponseType(typeof(OrderDto), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<OrderDto>> GetByIdAsync([FromQuery] string id)
    {
        try
        {
            var order = await _orderService.GetByIdAsync(id);
            Log.Information($"Successfully got order with id {id}");
            var newOrderDto = new OrderDto(order.OrderTime, order.OrderTotal, order.CustomerId, order.Status, order.Products);

            return Ok(newOrderDto);  // Ok();
        }
        catch (Exception e)
        {
            Log.Error($"Failed to get order with id {id}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<Order>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }

    [HttpPut]
    [ProducesResponseType(typeof(IActionResult), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> CancelOrder([FromQuery] string id)
    {
        try
        {
            var newstatus = await _orderService.CancelOrder(id);
            Log.Information($"Successfully changed the status of the order with id {id}");
            return Ok();
        }
        catch (Exception e)
        {
            Log.Error($"Failed to delete rule with id {id}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<IActionResult>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }
}