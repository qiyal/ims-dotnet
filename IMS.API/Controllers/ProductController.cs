using System.Net;
using Microsoft.AspNetCore.Mvc;
using IMS.API.DTOs;
using FalconFlexCommon.Extensions;

// using IMS.API.Extensions;
using IMS.API.RequestModels;
using IMS.Domain.Entities.Products;
using IMS.API.Services.Interfaces;
using Serilog;
using SnoonuCommons.Helpers;

namespace IMS.API.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class ProductController : Controller
{
    private readonly IProductService _productService;

    public ProductController(IProductService productService)
    {
        _productService = productService;
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(ProductDto), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<ProductDto>> CreateAsync([FromBody] ProductDto productDto)
    {
        try
        {
            var product = await _productService.CreateAsync(productDto);
            Log.Information($"Successfully created dataSource with id: {product.Id}");
            
            return Ok(productDto);
        }
        catch (Exception e)
        {
            Log.Error($"Failed to create dataSource. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(e.Message);
        }
    }
    
    
    [HttpPut]
    [ProducesResponseType(typeof(UpdateProductRequest), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<UpdateProductRequest>> UpdateAsync([FromBody] UpdateProductRequest updateProductRequest)
    {
        try
        {
            await _productService.UpdateProductDetails(updateProductRequest);
            Log.Information($"Successfully updated product with id: {updateProductRequest.Id}");
            var product = await _productService.GetByIdAsync(updateProductRequest.Id);

            return Ok(updateProductRequest);
        }
        catch (Exception e)
        {
            Log.Error($"Failed to update product with id {updateProductRequest.Id}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(e.Message);
        }
    }

    /* [HttpGet]
    [ProducesResponseType(typeof(ProductDto), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<Product>> GetByIdAsync([FromQuery] string id)
    {
        try
        {
            var product = await _productService.GetByIdAsync(id);
            Log.Information($"Successfully got product with id {id}");
            return Ok(product);  // Ok(product.ToDto());
        }
        catch (Exception e)
        {
            Log.Error($"Failed to get product with id {id}. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<Product>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }

    [HttpGet("get-all")]
    [ProducesResponseType(typeof(Product), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<Product>> GetAllAsync([FromQuery] ProductPagingFilteringModel productPagingFilteringModel)
    {
        try
        {
            var products = await _productService
                .GetAllAsync(productPagingFilteringModel.CategoryId,
                    productPagingFilteringModel.UserId,
                    productPagingFilteringModel.GetPageIndex(),
                    productPagingFilteringModel.GetPageSize());

            var result = new ProductListModel();
            result.PagingFilteringContext.LoadPagedList(products);
            //result.Products = products.Select(product => product.ToDto()).ToList();
            result.Products = products.ToList();
            Log.Information("Successfully got all action logs");
            return Ok(result);

        }
        catch (Exception e)
        {
            Log.Error($"Failed to get all action logs. Error: {e.Message}");
            Log.Error(e.StackTrace);
            return BadRequest(CreateResponse<ProductListModel>.CreateErrorResponse(
                HttpStatusCode.BadRequest.ToString(),
                e.Message));
        }
    }*/
}