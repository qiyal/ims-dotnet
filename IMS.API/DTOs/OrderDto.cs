using IMS.API.DTOs;
using IMS.Domain.Entities.Orders;
using IMS.Domain.Enums;
namespace IMS.API.DTOs;

public class OrderDto
{
    public DateTime OrderTime { get; set; }
    public decimal OrderTotal { get; set; }
    public string CustomerId { get; set; } 
    public OrderStatus Status { get; set; }
    public List<ProductOrderDetail> Products { get; set; }
    
    public OrderDto(DateTime orderTime, decimal orderTotal, string customerId, OrderStatus status, List<ProductOrderDetail> products)
    {
        OrderTime = orderTime;
        OrderTotal = orderTotal;
        CustomerId = customerId;
        Status = status;
        Products = products;
    }
}