using IMS.Domain.Entities.Products;

namespace IMS.API.DTOs;

public class ProductDto
{
    public string ProductName { get; set; }
    public DateTime ExpirationDate { get; set; }
    public string SupplierId { get; set; }
    public string CategoryId { get; set; }
    public int Amount { get; set; }
    public int CostUnit { get; set; }
    
    public ProductDto(string productName, DateTime expirationDate, string supplierId, string categoryId, int amount, int costUnit)
    {
        ProductName = productName;
        ExpirationDate = expirationDate;
        SupplierId = supplierId;
        CategoryId = categoryId;
        Amount = amount;
        CostUnit = costUnit;
    }
}