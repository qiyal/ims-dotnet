using FalconFlexCommon.Mongo.DbContext;
using FalconFlexCommon.Mongo.Misc;
using FalconFlexCommon.Mongo.Repositories;
using IMS.Domain.Entities.Products;
using IMS.Domain.Entities.Users;
using IMS.Domain.Entities.Categories;
using IMS.Domain.Entities.Orders;
using IMS.Domain.Entities.Suppliers;

namespace IMS.API.HostedServices;

public class DatabaseInitHostedService : IHostedService
{
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public DatabaseInitHostedService(IServiceScopeFactory serviceScopeFactory)
    {
        _serviceScopeFactory = serviceScopeFactory;
    }
    
    public async Task StartAsync(CancellationToken cancellationToken)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var databaseContext = scope.ServiceProvider.GetRequiredService<IDatabaseContext>();
                var productRepository = scope.ServiceProvider.GetRequiredService<IRepository<Product>>();
                var orderRepository = scope.ServiceProvider.GetRequiredService<IRepository<Order>>();
                var userRepository = scope.ServiceProvider.GetRequiredService<IRepository<User>>();
                var categoryRepository = scope.ServiceProvider.GetRequiredService<IRepository<Category>>();

                
                //Ensure Collections Exists
                databaseContext.EnsureCollectionExists(nameof(Product));
                databaseContext.EnsureCollectionExists(nameof(Order));
                databaseContext.EnsureCollectionExists(nameof(User));
                databaseContext.EnsureCollectionExists(nameof(Category));
                databaseContext.EnsureCollectionExists(nameof(Supplier));

                
                // Product
                await databaseContext.CreateIndex(productRepository,
                    OrderBuilder<Product>.Create()
                        .Ascending(r => r.SupplierId)
                        .Ascending(r => r.CategoryId), "ProductCompositeIdx");

                   
                await databaseContext.CreateTtlIndex(productRepository,
                  OrderBuilder<Product>.Create()
                     .Descending(t => t.ExpirationDate), "productExpireAtUtcExpiryIdx", new TimeSpan(0, 0, 0, 0));
                
              
            }
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
}