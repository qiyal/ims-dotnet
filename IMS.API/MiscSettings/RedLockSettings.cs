namespace IMS.API.MiscSettings;

public class RedLockSettings
{
public string Host { get; set; }
public int Port { get; set; }
public bool Ssl { get; set; }
public string RedisKeyFormat { get; set; }
public string Password { get; set; }
}