using IMS.API.DTOs;
using IMS.Domain.Entities.Products;

namespace IMS.API.RequestModels;

public class ProductListModel
{
    public ProductListModel()
    {
        PagingFilteringContext = new ProductPagingFilteringModel();
        Products = new List<Product>();
    }

    public ProductPagingFilteringModel PagingFilteringContext { get; set; }
    public IList<Product> Products { get; set; }
}