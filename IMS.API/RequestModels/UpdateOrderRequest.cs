using System.ComponentModel.DataAnnotations;
using IMS.API.DTOs;
using IMS.Domain.Entities.Orders;
using IMS.Domain.Enums;

namespace IMS.API.RequestModels;

public record UpdateOrderRequest
{
    [Required] public string Id { get; set; }
    [Required] public string SummaryOfChanges { get; set; }
    public OrderStatus OrderStatus { get; set; }
    public List<ProductOrderDetail> Products { get; set; }
    public decimal OrderTotal { get; set; }
    //public DeduplicationInfoDto? DeduplicationInfoDto { get; set; }
    // public PostProcessingInfoDto? PostProcessingInfoDto { get; set; }
}