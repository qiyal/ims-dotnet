using System.ComponentModel.DataAnnotations;
using IMS.API.DTOs;

namespace IMS.API.RequestModels;

public record UpdateProductRequest
{
    [Required] public string Id { get; set; }
    [Required] public string SummaryOfChanges { get; set; }
    public string ProductName { get; set; }
    public DateTime ExpirationDate { get; set; }
    public int Amount { get; set; }
    public int CostUnit { get; set; }
    //public DeduplicationInfoDto? DeduplicationInfoDto { get; set; }
    // public PostProcessingInfoDto? PostProcessingInfoDto { get; set; }
}