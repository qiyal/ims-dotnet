using System.Data;
using FalconFlexCommon.Mongo.Pagination;
using IMS.API.Services.Interfaces;
using IMS.Domain;
using IMS.Domain.Entities.Categories;
using IMS.Domain.UnitOfWork.Interfaces;


namespace IMS.API.Services;

public class CategoryService : ICategoryService
{
    private readonly IUnitOfWork _unitOfWork;
    public CategoryService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    
    public async Task<Category> CreateAsync(string categoryName, string categoryDescription)
    {
        var category = new Category(categoryName, categoryDescription);
        return await _unitOfWork.CategoryRepository.InsertAsync(category);
    }

    public async Task<Category> GetByIdAsync(string id)
    {
        return await _unitOfWork.CategoryRepository.GetByIdAsync(id);
    }
        
    public async Task<IPagedList<Category>> GetAllAsync(int pageIndex = 0, int pageSize = Int32.MaxValue)
    {
        var query = _unitOfWork.CategoryRepository.Table;
        return await PagedList<Category>.Create(query, pageIndex, pageSize);
    }
}