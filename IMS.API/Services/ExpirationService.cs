
using System.Data;
using FalconFlexCommon.Mongo.Pagination;
using IMS.API.Services.Interfaces;
using IMS.Domain;
using IMS.Domain.Entities.Products;
using IMS.Domain.UnitOfWork.Interfaces;

namespace IMS.API.Services;

public class ExpirationService : IExpirationService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IProductService _productService;

    public ExpirationService(IUnitOfWork unitOfWork, IProductService productService)
    {
        _unitOfWork = unitOfWork;
        _productService = productService;
    }

    public async Task<bool> CheckExpired(string productId)
    {
        var product = await _productService.GetByIdAsync(productId);
        if (product == null) throw new ArgumentException("Order not found.");
        return (product.ExpirationDate >= DateTime.Today);
    }   

    public async Task<int> RemoveExpired()
    {
        var allProducts = await _productService.GetAllAsync();
        int count = 0;
        int costAmount = 0;
        foreach (var product in allProducts)
        {
            var expired = CheckExpired(product.Id);
            if (expired.Result)
            {
                try
                {
                    var productToNullify = await _productService.GetByIdAsync(product.Id);
                    var amount = product.Amount;
                    productToNullify.Amount = 0;
                    await _unitOfWork.ProductRepository.UpdateAsync(productToNullify);
                    count += amount;
                    costAmount += amount * product.CostUnit;
                }
                catch (Exception e)
                {
                    throw new ArgumentException($"Can't update the product quantity.");
                    return 0;
                }
            }
        }
        // log here maybe?
        Console.WriteLine(count + "products Removed from the repository with value" + costAmount);
        return count;
    }
}