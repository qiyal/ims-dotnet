using FalconFlexCommon.Mongo.Pagination;
using IMS.Domain.Entities.Categories;

namespace IMS.API.Services.Interfaces;

public interface ICategoryService
{
    public Task<Category> CreateAsync(string categoryName, string categoryDescription);
    public Task<Category> GetByIdAsync(string id);
    public Task<IPagedList<Category>> GetAllAsync(int pageIndex = 0, int pageSize = Int32.MaxValue);
}