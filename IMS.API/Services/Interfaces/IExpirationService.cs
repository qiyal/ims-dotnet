using FalconFlexCommon.Mongo.Pagination;
using IMS.Domain.Entities.Products;

namespace IMS.API.Services.Interfaces;

public interface IExpirationService
{
    public Task<bool> CheckExpired(string productId);
    public Task<int> RemoveExpired(); 
}