using FalconFlexCommon.Mongo.Pagination;
using IMS.Domain.Enums;
using IMS.API.DTOs;
using IMS.API.RequestModels;
using MongoDB.Entities;
using Order = IMS.Domain.Entities.Orders.Order;

namespace IMS.API.Services.Interfaces;

public interface IOrderService
{
    public Task<Order> CreateAsync(OrderDto orderDto);
    public Task<Order> GetByIdAsync(string id);
    public Task<bool> FulfillOrder(string orderId);
    public Task<OrderStatus> CancelOrder(string orderId);
    public Task<Order> UpdateOrderDetails(UpdateOrderRequest orderDetails);
    public Task<IPagedList<Order>> GetAllAsync(
        int pageIndex = 0, int pageSize = Int32.MaxValue);
}