using FalconFlexCommon.Mongo.Pagination;
using IMS.Domain.Entities.Products;
using IMS.API.DTOs;
using IMS.API.RequestModels;
namespace IMS.API.Services.Interfaces;

public interface IProductService
{
    public Task<Product> CreateAsync(ProductDto productDto);
    public Task<Product> GetByIdAsync(string id);
    public Task<int> GetProductAmount(string productId);
    public Task<Product> UpdateQuantities(string productId, int toDiscard);
    public Task<Product> UpdateProductDetails(UpdateProductRequest productDetails);
    public Task<IPagedList<Product>> GetAllAsync(
        string? supplierId = null,
        string? categoryId = null,
        int pageIndex = 0, int pageSize = Int32.MaxValue);
}