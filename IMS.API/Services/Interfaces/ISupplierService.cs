using FalconFlexCommon.Mongo.Pagination;
using IMS.Domain.Entities.Suppliers;

namespace IMS.API.Services.Interfaces;

public interface ISupplierService
{
    public Task<Supplier> CreateAsync(string supplierName, string supplierEmail, string supplierCountry, string supplierAddress);
    public Task<Supplier> GetByIdAsync(string id);
    public Task<IPagedList<Supplier>> GetAllAsync(int pageIndex = 0, int pageSize = Int32.MaxValue);
}