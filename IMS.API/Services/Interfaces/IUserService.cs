using FalconFlexCommon.Mongo.Pagination;
using IMS.Domain.Entities.Users;

namespace IMS.API.Services.Interfaces;

public interface IUserService
{
    public Task<User> CreateAsync(string name, string email, string accessLevel);
    public Task<User> GetByIdAsync(string id);
    public Task<IPagedList<User>> GetAllAsync(
        int pageIndex = 0, int pageSize = Int32.MaxValue);
}