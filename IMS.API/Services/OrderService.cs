using FalconFlexCommon.Mongo.Pagination;
using IMS.API.DTOs;
using IMS.API.Services.Interfaces;
using IMS.Domain;
using IMS.Domain.Entities.Orders;
using IMS.Domain.UnitOfWork.Interfaces;
using IMS.Domain.Enums;
using IMS.API.RequestModels;


namespace IMS.API.Services;

public class OrderService : IOrderService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IProductService _productService;

    public OrderService(IUnitOfWork unitOfWork, IProductService productService)
    {
        _unitOfWork = unitOfWork;
        _productService = productService;
    }
    
    
    public async Task<Order> CreateAsync(OrderDto orderDto)
    {
        // validate first
        if (!orderDto.Products.Any())
        {
            throw new ArgumentException("Order must contain at least one product.", nameof(orderDto.Products));
        }
        bool allProductsAvailable = true;
        foreach (var detail in orderDto.Products)
        {
            var stock = await _productService.GetProductAmount(detail.ProductId); 
            if (detail.Quantity > stock)
            {
                allProductsAvailable = false;
                break;
            }
        }
        // create order
        if (allProductsAvailable)
        {
            var order = new Order(orderDto.OrderTime, orderDto.OrderTotal, orderDto.CustomerId, OrderStatus.UnderProcessing, orderDto.Products);
            return await _unitOfWork.OrderRepository.InsertAsync(order);

        }
        throw Exceptions.InvalidCreateOrder;
    }
    
    public async Task<bool> FulfillOrder(string orderId)
    {

        var order = await GetByIdAsync(orderId);
        if (order == null) throw new ArgumentException("Order not found.");

        foreach (var det in order.Products)
        {
            try
            {
                await _productService.UpdateQuantities(det.ProductId, -det.Quantity);
            }
            catch (Exception e)
            {
                throw new ArgumentException($"Can't update product quantity.", e);
            }
        }

        order.Status = OrderStatus.ReadyForDeliver; // Done!
        await _unitOfWork.OrderRepository.UpdateAsync(order);

        return true;
    }
    
    public async Task<OrderStatus> CancelOrder(string orderId)
    {
        var order = await _unitOfWork.OrderRepository.GetByIdAsync(orderId);

        // if processed, no
        switch(order.Status)
        {
            case OrderStatus.UnderProcessing:
            {
                break;
            }
            case OrderStatus.ReadyForDeliver:
            {
                foreach (var det in order.Products)
                {
                    // add back the products
                    try
                    { 
                        await _productService.UpdateQuantities(det.ProductId, det.Quantity);
                    }
                    catch (Exception e)
                    {
                        throw new ArgumentException($"Can't update product quantity.", e);
                    }
                }
                break;
            }
            case OrderStatus.Delivering:
            {
                foreach (var det in order.Products)
                {
                    // add back the products
                    try
                    {
                        await _productService.UpdateQuantities(det.ProductId, det.Quantity);
                    }
                    catch (Exception e)
                    {
                        throw new ArgumentException($"Can't update product quantity.", e);
                    }
                }
                break;
            }
            case OrderStatus.Delivered:
            {
                throw Exceptions.InvalidCancelOrder;
            }
            default: break;
        }
        order.Status = OrderStatus.Cancelled;
        await _unitOfWork.OrderRepository.UpdateAsync(order); 
        return order.Status;
    }
    
    public async Task<Order> UpdateOrderDetails(UpdateOrderRequest orderDetails)
    {
        var order = await _unitOfWork.OrderRepository.GetByIdAsync(orderDetails.Id);
        if (order == null)
        {
            throw Exceptions.ProductNotFoundException;
        } ;
       
        
        order.Status = orderDetails.OrderStatus;
        order.Products = orderDetails.Products;
        order.OrderTotal = orderDetails.OrderTotal;

        return await _unitOfWork.OrderRepository.UpdateAsync(order);
    }
    public async Task<Order> GetByIdAsync(string id)
    {
        return await _unitOfWork.OrderRepository.GetByIdAsync(id);
    }
        
    public async Task<IPagedList<Order>> GetAllAsync(int pageIndex = 0, int pageSize = Int32.MaxValue)
    {
        var query = _unitOfWork.OrderRepository.Table;

        query = query.OrderByDescending(order => order.OrderTime);
        return await PagedList<Order>.Create(query, pageIndex, pageSize);
    }
}