using System.Data;
using FalconFlexCommon.Mongo.Pagination;
using IMS.API.Services.Interfaces;
using IMS.Domain;
using IMS.Domain.Entities.Products;
using IMS.Domain.UnitOfWork.Interfaces;
using IMS.API.DTOs;
using IMS.API.RequestModels;
namespace IMS.API.Services;

public class ProductService : IProductService
{
    private readonly IUnitOfWork _unitOfWork;

    public ProductService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    
    public async Task<Product> CreateAsync(ProductDto productDto)
    {
        //await _unitOfWork.CommitAsync();
        var product = new Product(productDto.ProductName, productDto.SupplierId,
            productDto.CategoryId, productDto.ExpirationDate, productDto.Amount, productDto.CostUnit);
        return await _unitOfWork.ProductRepository.InsertAsync(product);
    }
    
    // use - sign with quantityChange to decrease the amount
    public async Task<Product> UpdateQuantities(string productId, int quantityChange)
    {
        var product = await _unitOfWork.ProductRepository.GetByIdAsync(productId);
        if (product == null)
        {
            throw new ArgumentException("Product doesn't exist", nameof(productId));
        }

        if (quantityChange < 0 && product.Amount < -quantityChange)
        {
            throw new ArgumentException("Product amount can't go below 0.");
        }

        product.Amount = product.Amount + quantityChange;
        return await _unitOfWork.ProductRepository.UpdateAsync(product);
    }
    public async Task<Product> UpdateProductDetails(UpdateProductRequest productDetails)
    {
        var product = await _unitOfWork.ProductRepository.GetByIdAsync(productDetails.Id);
        if (product == null)
        {
            throw Exceptions.ProductNotFoundException;
        } ;
        product.ProductName = productDetails.ProductName;
        product.ExpirationDate = productDetails.ExpirationDate;
        product.Amount = productDetails.Amount;
        product.CostUnit = productDetails.CostUnit;

        return await _unitOfWork.ProductRepository.UpdateAsync(product);
    }
    
    public async Task<int> GetProductAmount(string productId)
    {
        var product = await _unitOfWork.ProductRepository.GetByIdAsync(productId);
        return product?.Amount ?? 0;
    }
    public async Task<Product> GetByIdAsync(string id)
    {
        return await _unitOfWork.ProductRepository.GetByIdAsync(id);
    }
        
    public async Task<IPagedList<Product>> GetAllAsync(string? supplierId = null,
        string? categoryId = null, int pageIndex = 0, int pageSize = Int32.MaxValue)
    {
        var query = _unitOfWork.ProductRepository.Table;
        if (supplierId != null)
        {
            query = query.Where(product => product.SupplierId == supplierId);
        }
        
        if (categoryId != null)
        {
            query = query.Where(product => product.CategoryId == categoryId);
        }
        
        query = query.OrderByDescending(product => product.CreatedAtUtc);
        return await PagedList<Product>.Create(query, pageIndex, pageSize);
    }
}