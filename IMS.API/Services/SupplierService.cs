using System.Data;
using FalconFlexCommon.Mongo.Pagination;
using IMS.API.Services.Interfaces;
using IMS.Domain;
using IMS.Domain.Entities.Suppliers;
using IMS.Domain.UnitOfWork.Interfaces;


namespace IMS.API.Services;

public class SupplierService : ISupplierService
{
    private readonly IUnitOfWork _unitOfWork;
    public SupplierService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    
    public async Task<Supplier> CreateAsync(string supplierName, string supplierEmail, string supplierCountry, string supplierAddress)
    {
        var supplier = new Supplier(supplierName, supplierEmail, supplierCountry, supplierAddress);
        return await _unitOfWork.SupplierRepository.InsertAsync(supplier);
    }

    public async Task<Supplier> GetByIdAsync(string id)
    {
        return await _unitOfWork.SupplierRepository.GetByIdAsync(id);
    }
        
    public async Task<IPagedList<Supplier>> GetAllAsync(int pageIndex = 0, int pageSize = Int32.MaxValue)
    {
        var query = _unitOfWork.SupplierRepository.Table;
        return await PagedList<Supplier>.Create(query, pageIndex, pageSize);
    }
}