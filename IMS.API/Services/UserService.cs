using System.Data;
using FalconFlexCommon.Mongo.Pagination;
using IMS.API.Services.Interfaces;
using IMS.Domain;
using IMS.Domain.Entities.Users;
using IMS.Domain.UnitOfWork.Interfaces;


namespace IMS.API.Services;

public class UserService : IUserService
{
    private readonly IUnitOfWork _unitOfWork;
    public UserService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    
    public async Task<User> CreateAsync(string name, string email, string accessLevel)
    {
        var user = new User(name, email, accessLevel);
        return await _unitOfWork.UserRepository.InsertAsync(user);
    }

    public async Task<User> GetByIdAsync(string id)
    {
        return await _unitOfWork.UserRepository.GetByIdAsync(id);
    }
        
    public async Task<IPagedList<User>> GetAllAsync(int pageIndex = 0, int pageSize = Int32.MaxValue)
    {
        var query = _unitOfWork.UserRepository.Table;
        return await PagedList<User>.Create(query, pageIndex, pageSize);
    }
}