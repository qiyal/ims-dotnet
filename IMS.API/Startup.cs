using FalconFlexCommon.Extensions;
using FalconFlexCommon.MassTransit.Constants;
using FalconFlexCommon.Mongo;
using FalconFlexCommon.Redis;
using FalconFlexCommon.Swagger;
using FalconFlexCommon.TypeSearchers;
using FalconFlexCommon.UserContext;
using IMS.API.HostedServices;
using IMS.Domain.UnitOfWork;
using IMS.Domain.UnitOfWork.Interfaces;
using IMS.API.Services.Interfaces;
using IMS.API.Services;
using IMS.API.MiscSettings;


using MassTransit;

using Serilog;
using static FalconFlexCommon.AutoMapper.Extensions;

namespace IMS.API;

public class Startup
{
    private readonly IConfiguration _configuration;

    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddCustomMvc();
        services.AddCustomAuth();
        services.AddSwaggerDocs();
        services.AddMongo();
        services.AddTypeSearcher();
        services.AddWorkContext();
        services.AddHttpClient();
        // services.AddLazyCache();
        services.AddRedisClient();
        services.AddHealthChecks();

        //
        services.StartupConfig<RedLockSettings>(_configuration.GetSection("redLockSettings"));
        // services.StartupConfig<SlackNotificationSettings>(_configuration.GetSection("slackNotificationSettings"));
        
        // Add services to the container.
        services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));
        // services.AddScoped<IConnectionFactoryService, ConnectionFactoryService>();
        services.AddScoped<IProductService, ProductService>();
        services.AddScoped<IOrderService, OrderService>();
        // services.AddScoped<IDaService, DataSourceService>();
        // services.AddScoped<IDeduplicationService, DeduplicationService>();
        // services.AddScoped<IMatchService, MatchService>();
        // services.AddScoped<IActionLogService, ActionLogService>();
        // services.AddScoped<IRuleProcessingService, RuleProcessingService>();
        

        #region Database Init
        services.AddHostedService<DatabaseInitHostedService>();
        #endregion
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider services,
        ITypeSearcher typeSearcher, RedLockSettings redLockSettings, IHostApplicationLifetime applicationLifetime) // removed RedLockSettings redLockSettings from args
    {
        // Configure the HTTP request pipeline.
        if (env.IsDevelopment())
        {
            Console.WriteLine("here");
            app.UseSwagger();
            
            app.UseSwaggerUI();
        }

        // RedLock.Init(redLockSettings);
        AddAutoMapper(typeSearcher);
        Console.WriteLine("here 2");
        app.UseSwaggerDocs();
        app.UseServiceId();
        app.UseSerilogRequestLogging();
        app.UseRouting();
        app.UseAuthentication();
        app.UseAuthorization();
        app.UseWorkContext();
        applicationLifetime.ApplicationStopping.Register(OnShutdown);
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
            endpoints.MapHealthChecks("/health");
        });
    }
    private void OnShutdown()
    {
        // RedLock.Dispose();
    }
}