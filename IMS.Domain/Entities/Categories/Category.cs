using MongoDB.Bson;

namespace IMS.Domain.Entities.Categories;
using FalconFlexCommon.Mongo.BaseClasses;


public class Category : BaseEntity
{
    public Category(
        string categoryName,
        string description
    )
    {
        SetCategoryName(categoryName);
        SetDescription(description);
    }
    
    public string CategoryName { get;  set; }
    public string Description  { get;  set; }

    private void SetCategoryName(string categoryName)
    {
        CategoryName = categoryName;
    }
    private void SetDescription(string description)
    {
        Description = description;
    }

}