using MongoDB.Bson;

namespace IMS.Domain.Entities.Orders;
using FalconFlexCommon.Mongo.BaseClasses;
using IMS.Domain.Entities.Orders;
using IMS.Domain.Enums;

public class Order : BaseEntity
{
    public Order(
        DateTime orderTime, 
        decimal orderTotal,
        string customerId,
        OrderStatus status,
        List<ProductOrderDetail> products
        )
    {
        SetOrderTime(orderTime);
        SetCustomerId(customerId);
        SetOrderTotal(orderTotal);
        SetStatus(status);
        SetProducts(products);
    }

    public List<ProductOrderDetail> Products { get; set; }
    public DateTime OrderTime { get; protected set; }
    
    public decimal OrderTotal { get; set; }
    public string CustomerId { get; protected set; }
    public OrderStatus Status { get;  set; }

    private void SetOrderTime(DateTime orderTime)
    {
        OrderTime = orderTime;
    }

    private void SetOrderTotal(decimal orderTotal)
    {
        OrderTotal = orderTotal;
    }
    
    private void SetStatus(OrderStatus status)
    {
        Status = status;
    }
    
    private void SetCustomerId(string customerId)
    {
        CustomerId = customerId ?? throw Exceptions.CustomerIdNullException;
    }


    private void SetProducts(List<ProductOrderDetail> products)
    {
        Products = products;
    }

}