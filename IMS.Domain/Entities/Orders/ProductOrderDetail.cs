using CSharpFunctionalExtensions;
namespace IMS.Domain.Entities.Orders;

public class ProductOrderDetail : ValueObject
{
    public string ProductId { get; }
    public int Quantity { get; }
    
    public ProductOrderDetail(string productId, int quantity)
    {
        ProductId = productId;
        Quantity = quantity;
    }

    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return ProductId;
        yield return Quantity;
    } 
}