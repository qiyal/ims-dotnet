using FalconFlexCommon.Mongo.BaseClasses;
using MongoDB.Bson;

namespace IMS.Domain.Entities.Products;

public class Product : BaseEntity
{
    public Product(
        string productName, 
        string supplierId, 
        string categoryId,
        DateTime expirationDate,
        int amount,
        int costUnit
        )
    {
        SetProductName(productName);
        SetSupplierId(supplierId);
        SetCategoryId(categoryId);
        SetExpirationDate(expirationDate);
        SetAmount(amount);
        SetCostUnit(costUnit);
    }

    public string ProductName { get; set; }
    public DateTime ExpirationDate { get; set; }
    public string SupplierId { get; protected set; }
    public string CategoryId { get; protected set; }
    public int Amount { get; set; }
    public int CostUnit { get; set; }

    private void SetProductName(string productName)
    {
        ProductName = productName;
    }

    private void SetSupplierId(string supplierId)
    {
        SupplierId = supplierId ?? throw Exceptions.SupplierIdNullException;
    }
    
    private void SetCategoryId(string categoryId)
    {
        CategoryId = categoryId ?? throw Exceptions.CategoryIdNullException;
    }

    private void SetExpirationDate(DateTime expirationDate)
    {
        ExpirationDate = expirationDate;
    }

    private void SetAmount(int amount)
    {
        Amount = amount;
    }
    private void SetCostUnit(int costUnit)
    {
        CostUnit = costUnit;
    }

}