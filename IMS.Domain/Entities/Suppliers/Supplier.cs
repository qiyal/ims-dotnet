using FalconFlexCommon.Mongo.BaseClasses;
namespace IMS.Domain.Entities.Suppliers;


public class Supplier : BaseEntity
{
    public Supplier(
        string supplierName,
        string supplierEmail,
        string supplierCountry,
        string supplierAddress
    )
    {
        SetSupplierName(supplierName);
        SetSupplierEmail(supplierEmail);
        SetSupplierCountry(supplierCountry);
        SetSupplierAddress(supplierAddress);
    }
    
    public string SupplierName { get; set; }
    public string SupplierEmail  { get; set; }
    public string SupplierCountry  { get; set; }
    public string SupplierAddress  { get; set; }

    private void SetSupplierName(string supplierName)
    {
        SupplierName = supplierName;
    }
    private void SetSupplierEmail(string supplierEmail)
    {
        // validate the email????
        SupplierEmail = supplierEmail;
    }
    private void SetSupplierCountry(string supplierCountry)
    {
        SupplierCountry = supplierCountry;
    }

    private void SetSupplierAddress(string supplierAddress)
    {
        SupplierAddress = supplierAddress;
    }

}