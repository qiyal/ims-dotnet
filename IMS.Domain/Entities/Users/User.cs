using FalconFlexCommon.Mongo.BaseClasses;
namespace IMS.Domain.Entities.Users;


public class User : BaseEntity
{
    public User(
        string name,
        string email,
        string accessLevel
    )
    {
        SetName(name);
        SetEmail(email);
        SetAccessLevel(accessLevel);
    }
    
    public string Name { get; set; }
    public string Email  { get; protected set; }
    public string AccessLevel  { get; protected set; }

    private void SetName(string name)
    {
        Name = name;
    }
    private void SetEmail(string email)
    {
        // validate the email????
        Email = email;
    }
    
    private void SetAccessLevel(string accessLevel)
    {
        AccessLevel = accessLevel;
    }

}