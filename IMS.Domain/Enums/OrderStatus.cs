namespace IMS.Domain.Enums
{
    public enum OrderStatus
    {
        UnderProcessing,
        ReadyForDeliver,
        Cancelled,
        // delivering set deliver status
        Delivering,
        Delivered
    }
}