namespace IMS.Domain;
public class Exceptions
{
    
    #region NotFoundExceptions
    
    public static readonly Exception ProductNotFoundException = 
        new("Product not found with given id");
    
    public static readonly Exception SupplierNotFoundException =
        new("Supplier not found with given id");
    
    public static readonly Exception UserNotFoundException =
        new("User not found with given id");
    
    public static readonly Exception CategoryNotFoundException =
        new("Category not found with given id");
    
    public static readonly Exception OrderNotFoundException =
        new("Order not found with given id");


    #endregion
    
    #region NullOrEmptyExceptions
    
    public static readonly Exception CustomerIdNullException = 
        new("Customer Id Cannot be Null or Empty");
    
    public static readonly Exception SupplierIdNullException = 
        new("Supplier Id Cannot be Null or Empty");
    
    public static readonly Exception CategoryIdNullException = 
        new("Category Id Cannot be Null or Empty");

    #endregion

    #region OrderExceptions
    public static readonly Exception InvalidCancelOrder = 
        new("Customer Id Cannot be Null or Empty");
    
    public static readonly Exception InvalidCreateOrder = 
        new("Order cannot be created");
    #endregion
}