using FalconFlexCommon.Mongo.Repositories;
using MongoDB.Driver;
using IMS.Domain.Entities.Categories;
using IMS.Domain.Repositories.Interfaces;

namespace IMS.Domain.Repositories;

public class CategoryRepository : ICategoryRepository
{
    private readonly IRepository<Category> _repository;

    public CategoryRepository(IRepository<Category> repository)
    {
        _repository = repository;
    }

    public async Task<Category> GetByIdAsync(string id)
    {
        var result= await _repository.GetByIdAsync(id);
        if (result == null) throw Exceptions.CategoryNotFoundException;
        return result;
    }

    public async Task<Category> InsertAsync(Category category, IClientSessionHandle? sessionHandle = null)
    {
        category.InitializeInsert("System", DateTime.UtcNow);
        return await _repository.InsertAsync(category, sessionHandle);
    }
    
    public IQueryable<Category> Table => _repository.Table;
}