using System.Linq.Expressions;
using MongoDB.Driver;
using IMS.Domain.Entities.Categories;

namespace IMS.Domain.Repositories.Interfaces;

public interface ICategoryRepository
{
    IQueryable<Category> Table { get; }
    Task<Category> GetByIdAsync(string id);
    Task<Category> InsertAsync(Category category, IClientSessionHandle? sessionHandle = null);

}