using System.Linq.Expressions;
using MongoDB.Driver;
using IMS.Domain.Entities.Orders;

namespace IMS.Domain.Repositories.Interfaces;

public interface IOrderRepository
{
    IQueryable<Order> Table { get; }
    Task<Order> GetByIdAsync(string id);
    Task<Order> InsertAsync(Order order, IClientSessionHandle? sessionHandle = null);
    Task<Order> UpdateAsync(Order order, IClientSessionHandle? sessionHandle = null);

}