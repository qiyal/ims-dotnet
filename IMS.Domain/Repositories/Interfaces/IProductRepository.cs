using System.Linq.Expressions;
using MongoDB.Driver;
using IMS.Domain.Entities.Products;

namespace IMS.Domain.Repositories.Interfaces;

public interface IProductRepository
{
    IQueryable<Product> Table { get; }
    Task<Product> GetByIdAsync(string id);
    Task<Product> InsertAsync(Product product, IClientSessionHandle? sessionHandle = null);
    Task<Product> UpdateAsync(Product product, IClientSessionHandle? sessionHandle = null);

}