using System.Linq.Expressions;
using MongoDB.Driver;
using IMS.Domain.Entities.Suppliers;

namespace IMS.Domain.Repositories.Interfaces;

public interface ISupplierRepository
{
    IQueryable<Supplier> Table { get; }
    Task<Supplier> GetByIdAsync(string id);
    Task<Supplier> InsertAsync(Supplier supplier, IClientSessionHandle? sessionHandle = null);
    Task<Supplier> UpdateAsync(Supplier supplier, IClientSessionHandle? sessionHandle = null);

}