using System.Linq.Expressions;
using MongoDB.Driver;
using IMS.Domain.Entities.Users;

namespace IMS.Domain.Repositories.Interfaces;

public interface IUserRepository
{
    IQueryable<User> Table { get; }
    Task<User> GetByIdAsync(string id);
    Task<User> InsertAsync(User user, IClientSessionHandle? sessionHandle = null);
    Task<User> UpdateAsync(User user, IClientSessionHandle? sessionHandle = null);

}