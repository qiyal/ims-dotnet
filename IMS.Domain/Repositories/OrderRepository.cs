using FalconFlexCommon.Mongo.Repositories;
using MongoDB.Driver;
using IMS.Domain.Entities.Orders;
using IMS.Domain.Repositories.Interfaces;

namespace IMS.Domain.Repositories;

public class OrderRepository : IOrderRepository
{
    private readonly IRepository<Order> _repository;

    public OrderRepository(IRepository<Order> repository)
    {
        _repository = repository;
    }

    public async Task<Order> GetByIdAsync(string id)
    {
        var result= await _repository.GetByIdAsync(id);
        if (result == null) throw Exceptions.OrderNotFoundException;
        return result;
    }

    public async Task<Order> InsertAsync(Order order, IClientSessionHandle? sessionHandle = null)
    {
        order.InitializeInsert("System", DateTime.UtcNow);
        return await _repository.InsertAsync(order, sessionHandle);
    }

    
    public async Task<Order> UpdateAsync(Order order, IClientSessionHandle? sessionHandle = null)
    {
        order.InitializeUpdate("System", DateTime.UtcNow);
        return await _repository.UpdateAsync(order, sessionHandle);
    }
    
    public IQueryable<Order> Table => _repository.Table;
}