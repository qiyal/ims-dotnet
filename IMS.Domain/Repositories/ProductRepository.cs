using FalconFlexCommon.Mongo.Repositories;
using FalconFlexCommon.UserContext;
using MongoDB.Driver;
using IMS.Domain.Entities.Products;
using IMS.Domain.Repositories.Interfaces;

namespace IMS.Domain.Repositories;

public class ProductRepository : IProductRepository
{
    private readonly IRepository<Product> _repository;

    public ProductRepository(IRepository<Product> repository)
    {
        _repository = repository;
    }

    public async Task<Product> GetByIdAsync(string id)
    {
        var result= await _repository.GetByIdAsync(id);
        if (result == null) throw Exceptions.ProductNotFoundException;
        return result;
    }

    public async Task<Product> InsertAsync(Product product, IClientSessionHandle? sessionHandle = null)
    {
        product.InitializeInsert("System", DateTime.UtcNow);
        return await _repository.InsertAsync(product, sessionHandle);
    }

    
    public async Task<Product> UpdateAsync(Product product, IClientSessionHandle? sessionHandle = null)
    {
        product.InitializeUpdate("System", DateTime.UtcNow);
        return await _repository.UpdateAsync(product, sessionHandle);
    }
    
    public IQueryable<Product> Table => _repository.Table;
}