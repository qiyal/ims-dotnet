using FalconFlexCommon.Mongo.Repositories;
using FalconFlexCommon.UserContext;
using MongoDB.Driver;
using IMS.Domain.Entities.Suppliers;
using IMS.Domain.Repositories.Interfaces;

namespace IMS.Domain.Repositories;

public class SupplierRepository : ISupplierRepository
{
    private readonly IRepository<Supplier> _repository;

    public SupplierRepository(IRepository<Supplier> repository)
    {
        _repository = repository;
    }

    public async Task<Supplier> GetByIdAsync(string id)
    {
        var result= await _repository.GetByIdAsync(id);
        if (result == null) throw Exceptions.SupplierNotFoundException;
        return result;
    }

    public async Task<Supplier> InsertAsync(Supplier supplier, IClientSessionHandle? sessionHandle = null)
    {
        supplier.InitializeInsert("System", DateTime.UtcNow);
        return await _repository.InsertAsync(supplier, sessionHandle);
    }

    
    public async Task<Supplier> UpdateAsync(Supplier supplier, IClientSessionHandle? sessionHandle = null)
    {
        supplier.InitializeUpdate("System", DateTime.UtcNow);
        return await _repository.UpdateAsync(supplier, sessionHandle);
    }
    
    public IQueryable<Supplier> Table => _repository.Table;
}