using FalconFlexCommon.Mongo.Repositories;
using FalconFlexCommon.UserContext;
using MongoDB.Driver;
using IMS.Domain.Entities.Users;
using IMS.Domain.Repositories.Interfaces;

namespace IMS.Domain.Repositories;

public class UserRepository : IUserRepository
{
    private readonly IRepository<User> _repository;

    public UserRepository(IRepository<User> repository)
    {
        _repository = repository;
    }

    public async Task<User> GetByIdAsync(string id)
    {
        var result= await _repository.GetByIdAsync(id);
        if (result == null) throw Exceptions.UserNotFoundException;
        return result;
    }

    public async Task<User> InsertAsync(User user, IClientSessionHandle? sessionHandle = null)
    {
        user.InitializeInsert("System", DateTime.UtcNow);
        return await _repository.InsertAsync(user, sessionHandle);
    }

    
    public async Task<User> UpdateAsync(User user, IClientSessionHandle? sessionHandle = null)
    {
        user.InitializeUpdate("System", DateTime.UtcNow);
        return await _repository.UpdateAsync(user, sessionHandle);
    }
    
    public IQueryable<User> Table => _repository.Table;
}