using MongoDB.Driver;
using IMS.Domain.Repositories.Interfaces;

namespace IMS.Domain.UnitOfWork.Interfaces;

public interface IUnitOfWork
{
    IProductRepository ProductRepository { get; }
    ICategoryRepository CategoryRepository { get; }
    ISupplierRepository SupplierRepository { get; }
    IOrderRepository OrderRepository { get; }
    IUserRepository UserRepository { get; }

    public Task RunInTransaction(Func<IClientSessionHandle, Task> transactionMethod);
}