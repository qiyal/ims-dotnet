using FalconFlexCommon.Mongo.DbContext;
using FalconFlexCommon.Mongo.Repositories;
using FalconFlexCommon.UserContext;
using MongoDB.Driver;
using IMS.Domain.Entities.Products;
using IMS.Domain.Entities.Orders;
using IMS.Domain.Entities.Users;
using IMS.Domain.Entities.Categories;
using IMS.Domain.Entities.Suppliers;

using IMS.Domain.Repositories;
using IMS.Domain.Repositories.Interfaces;
using IMS.Domain.UnitOfWork.Interfaces;

namespace IMS.Domain.UnitOfWork;

public class UnitOfWork : IUnitOfWork
{
    private readonly IMongoDatabase _database;
    private readonly IDatabaseContext _databaseContext;
    private IProductRepository _productRepository;
    private IOrderRepository _orderRepository;
    private ICategoryRepository _categoryRepository;
    private IUserRepository _userRepository;
    private ISupplierRepository _supplierRepository;

    private readonly IWorkContext _workContext;

    public UnitOfWork(IMongoDatabase database, IWorkContext workContext, IDatabaseContext databaseContext)
    {
        _database = database;
        _workContext = workContext; // when should i add this?
        _databaseContext = databaseContext;
    }


    public ICategoryRepository CategoryRepository
    {
        get { return _categoryRepository ??= new CategoryRepository(new Repository<Category>(_database)); }
    }

    public IUserRepository UserRepository
    {
        get { return _userRepository ??= new UserRepository(new Repository<User>(_database)); }
    }
    
    public IOrderRepository OrderRepository
    {
        get
        {
            return _orderRepository ??=
                new OrderRepository(new Repository<Order>(_database));
        }
    }
    
    public ISupplierRepository SupplierRepository
    {
        get
        {
            return _supplierRepository ??=
                new SupplierRepository(new Repository<Supplier>(_database));
        }
    }

    public IProductRepository ProductRepository
    {
        get
        {
            return _productRepository ??= new ProductRepository(new Repository<Product>(_database));
        }
    }

    public async Task RunInTransaction(Func<IClientSessionHandle, Task> transactionMethod)
    {
        await _databaseContext.RunInTransaction(transactionMethod);
    }
}